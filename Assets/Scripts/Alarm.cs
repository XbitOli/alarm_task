using System.Collections;
using UnityEngine;

public class Alarm : MonoBehaviour
{
    private Coroutine _alarmUp;

    private readonly float _stepIncreaseVolume = 0.1f;
    private readonly float _maxVolume = 1.0f;
    private readonly float _minValume = .0f;

    [SerializeField] private AudioSource _audioSource;
    [SerializeField] private float _speedUp;
    [SerializeField] private float _speedDown;
    
    public void PlayAudio()
    {
        if(!_audioSource.isPlaying)
            _audioSource.Play();

        _alarmUp = StartCoroutine(ChangeVolumeUp());
    }

    public void StopAudio()
    {
        StopCoroutine(_alarmUp);
        StartCoroutine(ChangeVolumeDown());
    }

    private IEnumerator ChangeVolumeUp()
    {
        for (float i = _audioSource.volume; i < _maxVolume; i += _stepIncreaseVolume * Time.deltaTime * _speedUp)
        {
            _audioSource.volume = i;
            yield return null;
        }
    }

    private IEnumerator ChangeVolumeDown()
    {
        do
        {
            _audioSource.volume -= _stepIncreaseVolume * Time.deltaTime * _speedDown;
            yield return null;
        } while (_audioSource.volume > _minValume);
        _audioSource.Stop();
    }
}
