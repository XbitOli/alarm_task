using UnityEngine;

public class ColorChanger : MonoBehaviour
{
    private SpriteRenderer _baseSpriteRenderer;

    [SerializeField] private SpriteRenderer _spriteRenderer;
    [SerializeField] private Color _targetColor;
    [SerializeField] private Color _baseColor;

    private void Start()
    {
        TryGetComponent(out _baseSpriteRenderer);
        _baseColor = _baseSpriteRenderer.color;
    }

    public void OnInsideChangeColor()
    {
        _spriteRenderer.color = _targetColor;
    }

    public void OnOutChangeColor()
    {
        _spriteRenderer.color = _baseColor;
    }
}
