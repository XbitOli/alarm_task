using UnityEngine;

public class Movement : MonoBehaviour
{
    [SerializeField] private float _speed;

    private void Update()
    {
        if(Input.GetKey(KeyCode.LeftArrow))
        {
            transform.Translate(new Vector3(-_speed * Time.deltaTime, 0, 0));
        }
        else if(Input.GetKey(KeyCode.RightArrow))
        {
            transform.Translate(new Vector3(_speed * Time.deltaTime, 0, 0));
        }
        else if(Input.GetKey(KeyCode.UpArrow))
        {
            transform.Translate(new Vector3(0, _speed * Time.deltaTime, 0));
        }
    }
}
